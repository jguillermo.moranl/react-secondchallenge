import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ValidadorComponent from './Components/validations/validations';
import CharComponent from './Components/charComponent/char';
export class App extends Component {
  state = {
    text: ''
  }

  handleText = (event) => {
    this.setState({ text: event.target.value });
  }

  handledeleteChar = (index) => {
    const letras = this.state.text.split('');
    letras.splice(index, 1);
    const text = letras.join('');
    this.setState({ text });
  }

  renderChartComponent = () => this.state.text.split('').map((datos, index) => <CharComponent key={index} letra={datos} click={() => this.handledeleteChar(index)} />)

  render() {
    return (
      <div className="content-page">
        <img src={logo} className="App-logo logo" alt="logo" />
        <div className="content-app">
          <input type="text" onChange={this.handleText} placeholder="Ingrese texto" value={this.state.text} />
          <ValidadorComponent lengthTexto={this.state.text.split('').length} />
          <p>{this.state.text}</p>
          {this.renderChartComponent()}
        </div>
      </div>
    );
  }
}

export default App;
