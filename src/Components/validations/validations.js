import React from 'react';
const ValidadorComponent = prop => prop.lengthTexto <= 5 ? <p>Texto demasiado corto</p> : <p>Texto lo suficientemente largo</p>;

export default ValidadorComponent;